package com.shenlong.mapper;

import com.shenlong.entity.User;
import org.apache.ibatis.annotations.Param;

/**
 * @Author:Jiajie
 * @Date:2020/8/10 16:50
 */
public interface UserMapper {
    User selectByUserName(@Param("username") String username);

    User selectByUserId(@Param("id") String id);
}
