package com.shenlong;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author:Jiajie
 * @Date:2020/8/10 16:50
 */
@SpringBootApplication
@MapperScan("com.shenlong.mapper")
public class SpringBootJwtApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootJwtApplication.class, args);
    }
}
