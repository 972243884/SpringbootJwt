package com.shenlong.service;

import com.shenlong.entity.User;

/**
 * @Author:Jiajie
 * @Date:2020/8/10 16:50 :
 */
public interface UserService {

    User getUserByUserName(String username);

    User getUserById(String id);
}
