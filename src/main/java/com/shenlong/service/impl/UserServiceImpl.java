package com.shenlong.service.impl;

import com.shenlong.entity.User;
import com.shenlong.mapper.UserMapper;
import com.shenlong.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author:Jiajie
 * @Date:2020/8/10 16:50
 */
@Service("UserService")
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public User getUserByUserName(String username) {
        return userMapper.selectByUserName(username);
    }

    @Override
    public User getUserById(String id) {
        return userMapper.selectByUserId(id);
    }
}
