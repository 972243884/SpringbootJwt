package com.shenlong.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.shenlong.entity.User;
import com.shenlong.service.TokenService;
import org.springframework.stereotype.Service;

/**
 * @Author:Jiajie
 * @Date:2020/8/1016:50 TODO:
 */
@Service("TokenService")
public class TokenServiceImpl implements TokenService {
    //生成Token
    @Override
    public String getToken(User user) {
        String token = "";
        token = JWT.create().withAudience(user.getId())//将user id保存到token里面
                .sign(Algorithm.HMAC256(user.getPassword()));//以password作为token的密钥
        return token;
    }
}
